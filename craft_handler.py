"""Checks and categorises craft files

Categorisations:
 - Vessel name
 - Upsilon name
 - Vessel parent body
 - Vessel situation
 - Vessel fuel amount
 - Vessel docking ports
 - Vessel claw
 - Vessel drills
 - Vessel ISRU
 - Vessel kerbals
 - Vessel kerbal capacity
 - Vessel part count

Checks:
 - Mods - exit immediately if incompatable mods found to avoid errors
 - Craft flight path (Module trip loggers)
 - Vessels are in valid (stable) orbits (handled by situation)
 - Situation ORBITING or LANDED
 - Vessels orbit (or are landed on) existing bodies
 - Kerbals present are not part of the OG four
 - Upsilon in vessel name - done in find_crafts
 - Terrain detail - uncheckable, game setting
 - Difficulty checks
 - Kerbal capacity checks

Cases:
 - No vessels
 - Vessel is flag
 - Vessel is asteroid
 - Vessel is debris

"""
# pylint: disable=logging-format-interpolation
# Partially based on scripts of OndrikB

import json
import logging
import re
from collections import OrderedDict

SOLAR_SYSTEM = {
    "Sun": dict(parent=None, id=0, has_atmo=True),
    "Moho": dict(parent="Sun", id=4, has_atmo=False),
    "Eve": dict(parent="Sun", id=5, has_atmo=True),
    "Gilly": dict(parent="Eve", id=13, has_atmo=False),
    "Kerbin": dict(parent="Sun", id=1, has_atmo=True),
    "Mun": dict(parent="Kerbin", id=2, has_atmo=False),
    "Minmus": dict(parent="Kerbin", id=3, has_atmo=False),
    "Duna": dict(parent="Sun", id=6, has_atmo=True),
    "Ike": dict(parent="Duna", id=7, has_atmo=False),
    "Dres": dict(parent="Sun", id=15, has_atmo=False),
    "Jool": dict(parent="Sun", id=8, has_atmo=True),
    "Laythe": dict(parent="Jool", id=9, has_atmo=True),
    "Vall": dict(parent="Jool", id=10, has_atmo=False),
    "Tylo": dict(parent="Jool", id=11, has_atmo=False),
    "Bop": dict(parent="Jool", id=12, has_atmo=False),
    "Pol": dict(parent="Jool", id=14, has_atmo=False),
    "Eeloo": dict(parent="Sun", id=16, has_atmo=False)
}

CAPACITIES = {
    "Mark1Cockpit": 1,
    "Mark2Cockpit": 1,
    "mk2Cockpit.Standard": 2,
    "mk2Cockpit.Inline": 2,
    "mk3Cockpit.Shuttle": 4,
    "mk1pod.v2": 1,
    "mk1-3pod": 3,
    "landerCabinSmall": 1,
    "mk2LanderCabin.v2": 2,
    "cupola": 1,
    "seatExternalCmd": 1,
    "mk3CrewCabin": 16,
    "mk2CrewCabin": 4,
    "MK1CrewCabin": 2,
    "crewCabin": 4,
    "Large.Crewed.Lab": 2
}

# mass per unit resource
DENSITIES = {
    "LiquidFuel": 5,
    "Oxidizer": 5,
    "IntakeAir": 5,
    "SolidFuel": 7.5,
    "MonoPropellant": 4,
    "XenonGas": 0.1,
    "Ore": 10,
    "EVA Propellant": 0,
    "ElectricCharge": 0,
    "Ablator": 1
}

SIZES = {
    "none": "N",
    "tiny": "T",
    "small": "S",
    "medium": "M",
    "large": "L",
    "extra_large":  "X",
    "enormous": "E"
}

VALID_RESOURCES = (
    "LiquidFuel",
    "Oxidizer",
    "MonoPropellant",
    "XenonGas"
)

MOD_WHITELIST = json.load(open("mod_whitelist.json", "r"))

CLAW = "GrapplingDevice"
DRILLS = ("MiniDrill", "RadialDrill")
ISRUS = ("MiniISRU", "ISRU")

CREW_BLACKLIST = ("Jebediah Kerman", "Bill Kerman", "Bob Kerman", "Valentina Kerman")

DOCKING_PORTS = {
    "dockingPort3": 0.625,      # Junior docking port
    "dockingPort2": 1.25,       # "normal" docking port
    "dockingPortLarge": 2.5,    # Senior docking port
    "dockingPortLateral": 1.25, # lateral Mk1 docking port
    "mk2DockingPort": 1.25,     # Mk2 (plane) docking port
    "dockingPort1": 1.25        # Shielded docking port
}


MONO_THRESHHOLD = 6 # in tonnes
XENON_THRESHHOLD = 1  # in tonnes

OPEN_SUBMISSIONS = ["Moho"]

class Craft:
    """Contains a KSP craft, along with its attributes"""
    def __init__(self, craft_dict):
        self.raw = craft_dict
        self.name = craft_dict["name"]
        self.upsilon_name = None
        self.parent = int(craft_dict["ORBIT"]["REF"])
        self.situation = craft_dict["sit"]
        self.parent_name = None # defined in craft check to avoid dict lookup errors
        self.kerbals = dict(capacity=None, crew=[])
        self.invalidation_reasons = []

    def classify(self):
        """Classifies the craft"""
        crew_capacity = 0
        fuel = 0
        resources = {k: 0 for k in DENSITIES}
        special_parts = dict(claw=False, drill=False, isru=False)
        docking_ports = OrderedDict(((0.625, 0), (1.25, 0), (2.5, 0)))
        ordered_sizes = ("none", "tiny", "small", "medium", "large", "extra_large", "enormous")
        # thresholds are the n of thing required for each size. 0 is implicit 'none', while
        # anything larger than the last threshold is 'enormous'
        parts = self.raw["PART"]
        boundaries = OrderedDict((
            ("parts", dict(thresholds=(50, 150, 300, 500, 800), value=len(parts))),
            ("fuel", dict(thresholds=(10, 40, 200, 1000, 4000))),
            ("crew", dict(thresholds=(5, 10, 20, 50, 100)))
        ))
        for part in parts:
            # finds kerbals
            if "crew" in part:
                convert_to_list(part, "crew")
                for kerbal in part["crew"]:
                    self.kerbals["crew"].append(kerbal)
            # find resources
            if "RESOURCE" in part:
                convert_to_list(part, "RESOURCE")
                for resource in part["RESOURCE"]:
                    # multiplies by resource 'density' to allow a fair
                    resources[resource["name"]] += (DENSITIES[resource["name"]]
                                                    * float(resource["amount"]) / 1000)

            if part["name"] in CAPACITIES:
                crew_capacity += CAPACITIES[part["name"]]
            elif part["name"] == CLAW:
                if not special_parts["claw"]:
                    modules = part["MODULE"]
                    # only count klaws that are available
                    for module in modules:
                        if module["name"] == "ModuleGrappleNode":
                            if module["state"] in ("Ready", "Disabled"):
                                special_parts["claw"] = True
                                break
            elif part["name"] in DRILLS:
                special_parts["drill"] = True
            elif part["name"] in ISRUS:
                special_parts["isru"] = True
            elif part["name"] in DOCKING_PORTS:
                convert_to_list(part, "MODULE")
                for mod in part["MODULE"]:
                    if mod["name"] == "ModuleDockingNode":
                        if mod["state"] == "Ready":
                            # increment the number of docking ports, looking up the port size
                            docking_ports[DOCKING_PORTS[part["name"]]] += 1

        self.kerbals["capacity"] = crew_capacity
        # all valid fuel summed
        for resource_name, amount in resources.items():
            if resource_name in VALID_RESOURCES:
                fuel += amount

        boundaries["fuel"]["value"] = fuel
        boundaries["crew"]["value"] = len(self.kerbals["crew"])
        craft_stats = []
        for item in boundaries.values():
            # if the value is 0, use none size
            if item["value"] == 0:
                craft_stats.append(SIZES["none"])
            else:
                for size_count, threshold in enumerate(item["thresholds"]):
                    if threshold > item["value"]:
                        # add one to index to compensate for 0 size not in thresholds
                        craft_stats.append(SIZES[ordered_sizes[size_count + 1]])
                        break
                # is bigger than all thresholds
                else:
                    craft_stats.append(SIZES[ordered_sizes[-1]])

        drill_isru = []
        for item in ["drill", "isru"]:
            if special_parts[item]:
                drill_isru.append("1")
            else:
                drill_isru.append("0")

        dock_data = [str(min(9, v)) for v in docking_ports.values()]

        extra_resources = OrderedDict((
            ("k", special_parts["claw"]),
            ("x", resources["XenonGas"] >= XENON_THRESHHOLD),
            ("m", resources["MonoPropellant"] >= MONO_THRESHHOLD)
        ))

        upsilon_name = "{0}-{1}-{2}".format("".join(craft_stats), "".join(drill_isru),
                                            "".join(dock_data))
        if any(extra_resources.values()):
            upsilon_name += "-"
            for char, value in extra_resources.items():
                if value:
                    upsilon_name += char
        self.upsilon_name = upsilon_name

    def check(self):
        """Checks the craft"""
        situation = self.raw["sit"]
        if not any(self.parent == x["id"] for x in SOLAR_SYSTEM.values()):
        #if self.parent not in [x["id"] for x in SOLAR_SYSTEM.values()]:
            self.invalidation_reasons.append("Orbiting non-existent body")
            return
        # builds a reverse lookup dict to find the body name from id
        body_reverse = {v["id"]: k for k, v in SOLAR_SYSTEM.items()}
        self.parent_name = body_reverse[self.parent]

        if self.parent_name == "Sun":
            self.invalidation_reasons.append("Cannot orbit sun")
        if situation not in ("LANDED", "ORBITING"):
            self.invalidation_reasons.append("Vessel must be in a stable orbit or landed")
        if any(kerbal in self.kerbals["crew"] for kerbal in CREW_BLACKLIST):
            self.invalidation_reasons.append("Vessel must not contain one of the main four Kerbals")
        if len(self.kerbals["crew"]) * 4 > self.kerbals["capacity"]:
            self.invalidation_reasons.append("Your craft is not suitable for long-term habitation. "
                                             "A minimum of 4 crew capacity for each kerbal "
                                             "is required.")
        if self.parent_name not in OPEN_SUBMISSIONS and self.parent_name != "Sun":
            self.invalidation_reasons.append("Submissions for this body aren't open yet")

        # no checks required if vessel is landed on kerbin
        if self.parent_name == "Kerbin" and situation == "LANDED":
            return

        route_bodies = []
        body_parent = self.parent_name
        # find all parent bodies of the destination body until it reaches the sun
        # eg Tylo>Jool>Sun
        while SOLAR_SYSTEM[body_parent]["parent"] is not None:
            route_bodies.append(body_parent)
            body_parent = SOLAR_SYSTEM[body_parent]["parent"]
        trip_invalid = []
        trip_logs = []
        # finds all module trip loggers, and appends them to trip_logs
        for part in self.raw["PART"]:
            if "MODULE" in part:
                if not part["name"].startswith("kerbalEVA"):
                    convert_to_list(part, "MODULE")
                    for mod in part["MODULE"]:
                        if mod["name"] == "ModuleTripLogger":
                            if "0" in mod["Log"]:
                                trip_logs.append(mod["Log"]["0"])

        for trip in trip_logs:
            body_actions = {}
            # in case there is only one trip logger entry
            trip = convert_to_list(trip)
            # create a dict with the body name as the key and all the recorded actions around
            # that body as the values
            for log_entry in trip:
                action, body = log_entry.split(",")
                if not body in body_actions:
                    body_actions[body] = []
                body_actions[body].append(action)
            if self.parent_name not in body_actions:
                trip_invalid.append("No interaction with destination planet")
            for body, actions in body_actions.items():
                if "Land" in actions:
                    if SOLAR_SYSTEM[body]["has_atmo"]:
                        if "Flight" not in actions:
                            trip_invalid.append("Never flew on landed atmospheric body")
                    if "Suborbit" not in actions:
                        trip_invalid.append("Never suborbital on landed body")
                if ("Escape" not in actions and body != self.parent_name and body != "Sun"
                        and body != SOLAR_SYSTEM[self.parent_name]["parent"]):
                    trip_invalid.append("Never escaped body that is not the destination")

            # route checks
            if any(x not in body_actions for x in route_bodies):
                trip_invalid.append("Never visited body on way to destination")
            # these checks are ok as kerbin landed crafts are exempt (early exit)
            if "Kerbin" not in body_actions:
                trip_invalid.append("Craft did not originate from Kerbin")
            elif any(x not in body_actions["Kerbin"] for x in ["Flight", "Suborbit", "Orbit"]):
                trip_invalid.append("Never performed Kerbin ascent")
        # if there are any invalid trip logs, add the reasons to invalidation_reasons
        if trip_invalid:
            self.invalidation_reasons.append("Flight path invalid: {0}"
                                             .format(", ".join(set(trip_invalid))))


def convert_to_list(obj, key=None):
    """Helper function to deal with dynamic typing of parsed SFS"""
    if key is None:
        if not isinstance(obj, list):
            return [obj]
        return obj
    if not isinstance(obj[key], list):
        obj[key] = [obj[key]]


def find_crafts(parsed_savefile):
    """Finds all the crafts in a given savefile"""
    craft_searches = []
    if "VESSEL" in parsed_savefile["GAME"]["FLIGHTSTATE"]:
        convert_to_list(parsed_savefile["GAME"]["FLIGHTSTATE"], "VESSEL")
        for vessel in parsed_savefile["GAME"]["FLIGHTSTATE"]["VESSEL"]:
            if vessel["type"] not in ("Debris", "Flag", "SpaceObject"):
                craft_searches.append(Craft(vessel))
    return craft_searches


def check_mods(save_file):
    """Checks the mods used in the savefile"""
    warnings = dict(mods={}, settings={})
    valid = True
    loader_info = save_file["GAME"]["LoaderInfo"]
    regexp = re.compile(r"\d+(\.\d+){2,}")
    for mod in loader_info:
        if ((not regexp.search(mod) or "dll" in mod)
                and all(x not in mod for x in [".dat", ".cfg", ".md"])):
            mod_data = {}
            if loader_info[mod] == "True":
                mod_data["enabled"] = True
            else:
                mod_data["enabled"] = False
            if not any(mod_name in mod for mod_name in MOD_WHITELIST["valid"]):
                if (mod_data["enabled"]
                        and not any(mod_name in mod for mod_name in MOD_WHITELIST["warning"])):
                    mod_data["level"] = "critical"
                    valid = False
                else:
                    mod_data["level"] = "warning"
            else:
                mod_data["level"] = "info"
            warnings["mods"][mod] = mod_data
    if save_file["GAME"]["PARAMETERS"]["preset"] != "Normal":
        warnings["settings"]["difficulty"] = "warning"
    version = [int(x) for x in save_file["GAME"]["version"].split(".")]
    if version[0] != 1 or version[1] < 9:
        warnings["settings"]["version"] = "critical"
        valid = False
    # elif version[2] != 1:
    #     warnings["settings"]["version"] = "warning"
    return (warnings, valid)


def process_savefile(parsed_sfs):
    """Processes a savefile and all its crafts"""
    warnings, valid = check_mods(parsed_sfs)
    out_dict = dict(warnings=warnings, crafts={}, exit_invalid=not valid)
    if not valid:
        return out_dict

    crafts = find_crafts(parsed_sfs)
    for craft in crafts:
        LOGGER.debug("Checking {0}".format(craft.name))
        craft.classify()
        craft.check()
        out_dict["crafts"][craft.name] = craft.__dict__

    return out_dict

LOGGER = logging.getLogger("craft_check")
